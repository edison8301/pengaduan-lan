<?php

class UserController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/main';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('view'),
			'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','changePassword','setPassword'),
			'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('admin','delete'),
			'users'=>array('@'),
			),
			array('deny',  // deny all users
			'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];

			$model->password = CPasswordHelper::hashPassword($model->password);

			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('user/admin'));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('user/admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionSetPassword($id)
	{
		$model = $this->loadModel($id);

		$SetPasswordForm = new SetPasswordForm;

		if(isset($_POST['SetPasswordForm']))
		{
			$SetPasswordForm->attributes = $_POST['SetPasswordForm'];

			if($SetPasswordForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($SetPasswordForm->password_baru);
				$model->save();
				Yii::app()->user->setFlash('success','Password berhasil diperbarui');
				$this->redirect(array('user/setPassword','id'=>$id));
			}
		}

		$this->render('setPassword',array(
			'model'=>$model,
			'SetPasswordForm'=>$SetPasswordForm
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
		$model->attributes=$_GET['User'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	public function actionChangePassword()
	{
		if(User::isUnit())
		{
			$this->redirect(array('unit/changePassword'));
		}
		
		$model = User::model()->findByAttributes(array('username'=>Yii::app()->user->id));

		$ChangePasswordForm = new ChangePasswordForm;

		if(isset($_POST['ChangePasswordForm']))
		{
			$ChangePasswordForm->attributes = $_POST['ChangePasswordForm'];

			if($ChangePasswordForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($ChangePasswordForm->password_baru);
				$model->save();
				Yii::app()->user->setFlash('success','Password berhasil diperbarui');
				$this->redirect(array('user/changePassword'));
			}
		}

		$this->render('changePassword',array(
			'model'=>$model,
			'ChangePasswordForm'=>$ChangePasswordForm
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=User::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
