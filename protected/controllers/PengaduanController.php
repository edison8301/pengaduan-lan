<?php

class PengaduanController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/main';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','dilihat'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','export','exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('directDelete'),
				'expression'=>'User::isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dilihat = date('Y-m-d H:i:s');
		$model->update();

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Pengaduan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengaduan']))
		{
			$model->attributes=$_POST['Pengaduan'];
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s'); 

			if($model->save())
			{
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionDilihat($id)
	{
		$model = $this->loadModel($id);
		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dilihat = date('Y-m-d H:i:s');
		
		if($model->save())
		{
			Yii::app()->user->setFlash('success','Pengaduan sudah ditandai sudah dilihat');
		} else {
			Yii::app()->user->setFlash('danger','Pengaduan GAGAL ditandai');
		}
		
		$this->redirect(Yii::app()->request->urlReferrer);
	}


/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Pengaduan']))
{
$model->attributes=$_POST['Pengaduan'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}
	
	public function actionDirectDelete($id)
	{
		$model = $this->loadModel($id);
		if($model->delete())
		{
			Yii::app()->user->setFlash('success','Pengaduan berhasil dihapus');
		} else {
			Yii::app()->user->setFlash('danger','Pengaduan GAGAL dihapus');
		}

		$this->redirect(Yii::app()->request->urlReferrer);
	}
	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
	if(Yii::app()->request->isPostRequest)
	{
	// we only allow deletion via POST request
	$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
	if(!isset($_GET['ajax']))
	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	else
	throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Pengaduan');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Pengaduan('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Pengaduan']))
$model->attributes=$_GET['Pengaduan'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Pengaduan::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='pengaduan-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

	public function actionExport()
	{
		$this->layout = '//layouts/admin/column2';
		$this->render('export');
	}


	public function actionExportExcel()
	{

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$criteria = new CDbCriteria;
			$params = array();

			if(!empty($_POST['tanggal_awal'])) {
				$criteria->addCondition('waktu_dibuat>=:tanggal_awal');
				$params[':tanggal_awal'] = $_POST['tanggal_awal'];
			}

			if(!empty($_POST['tanggal_akhir'])) {
				$criteria->addCondition('waktu_dibuat<=:tanggal_akhir');
				$params[':tanggal_akhir'] = $_POST['tanggal_akhir'];
			}

			if(!empty($_POST['kategori'])) {
				$criteria->addCondition('id_kategori=:kategori');
				$params[':kategori'] = $_POST['kategori'];
			}

			if(!empty($_POST['status'])) {
				$criteria->addCondition('id_status=:status');
				$params[':status'] = $_POST['status'];
			}

			$criteria->params = $params;
			$criteria->order = 'id ASC';



			$PHPExcel = new PHPExcel();

			$PHPExcel->getActiveSheet()->getStyle('A3:J3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:J1');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "DATA PENGADUAN");
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'NO');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'KODE');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'NAMA');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'EMAIL');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'TELEPON');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'ID KATEOGI');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'KELUHAN');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'ID STATUS');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'WAKTU DILIHAT');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'WAKTU DIBUAT');
			

				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
			


			$i = 1;
			$kolom = 4;

			foreach(pengaduan::model()->findAll($criteria) as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->kode);
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->nama);
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->email);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->telepon);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->getKategori());
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->keluhan);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->getStatus());
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->waktu_dilihat);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->waktu_dibuat);
			

				$PHPExcel->getActiveSheet()->getStyle('A3:J'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:J'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A3:J'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
									
				$i++; $kolom++;
			}

			$PHPExcel->getActiveSheet()->getStyle('A3:J'.$kolom)->getAlignment()->setWrapText(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:J'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	
			$filename = time().'_Pengaduan.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}




}