<?php



class TanggapanController extends Controller

{

/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/

public $layout='//layouts/admin/main';



/**
* @return array action filters
*/

public function filters()

{

return array(

'accessControl', // perform access control for CRUD operations

);

}



	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/

	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('index','view','createDisposisi'),
		'users'=>array('@'),
		),
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
		'actions'=>array('create','update','dilihat'),
		'users'=>array('@'),
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('admin','delete','directDelete'),
		'expression'=>'User::isAdmin()',
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}



/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/

public function actionView($id)

{

$this->render('view',array(

'model'=>$this->loadModel($id),

));

}

	public function actionDirectDelete($id)
	{
		$model = $this->loadModel($id);
		if($model->delete())
		{
			Yii::app()->user->setFlash('success','Tanggapan/disposisi berhasil dihapus');
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}
	
	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/

	public function actionCreate($id_pengaduan)
	{
		$model=new Tanggapan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$model->id_pengaduan= $id_pengaduan;

		$pengaduan = Pengaduan::model()->findbyPk($id_pengaduan);
		if($pengaduan->waktu_dilihat==null)
		{
			date_default_timezone_set('Asia/Jakarta');
			$pengaduan->waktu_dilihat = date('Y-m-d H:i:s'); 
			$pengaduan->save();
		}

		if(isset($_POST['Tanggapan']))
		{

			$message = 'Pengaduan Anda Diterima';
			$model->attributes=$_POST['Tanggapan'];
			
			//$pengaduan = Pengaduan::model()->findByAttributes(array('id' => $_GET['id_pengaduan']));
			//$admin = User::model()->findByAttributes(array('username'=>Yii::app()->user->name));

			$model->username_pembuat = Yii::app()->user->id;
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s'); 

			if($model->save())
			{
				$model->sendMailToComplainer();
				$this->redirect(array('pengaduan/view','id'=>$model->id_pengaduan));
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));

	}

	public function actionCreateDisposisi()
	{

		$model=new Tanggapan;

		$model->scenario = 'disposisi';

		if(isset($_POST['Tanggapan']))
		{

			$message = 'Pengaduan Anda Diterima';
			$model->attributes=$_POST['Tanggapan'];

			$model->id_pengaduan= $_GET['id_pengaduan'];


			$model->username_pembuat= Yii::app()->user->id;
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s'); 

			if($model->save())
			{
				$model->sendMailToComplainer();
				$this->redirect(array('pengaduan/view','id'=>$model->id_pengaduan));
			}
		}

		$this->render('createDisposisi',array(
			'model'=>$model
		));
	}

	public function actionDilihat($id)
	{
		$model = $this->loadModel($id);
		date_default_timezone_set('Asia/Jakarta');
		$model->waktu_dilihat = date('Y-m-d H:i:s');
		
		if($model->save())
		{
			Yii::app()->user->setFlash('success','Tanggapan sudah ditandai sudah dilihat');
		} else {
			Yii::app()->user->setFlash('danger','Tanggapan GAGAL ditandai');
		}
		
		$this->redirect(Yii::app()->request->urlReferrer);
	}



/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @paraminteger $id the ID of the model to be updated
*/

public function actionUpdate($id)

{

$model=$this->loadModel($id);



// Uncomment the following line if AJAX validation is needed

// $this->performAjaxValidation($model);



if(isset($_POST['Tanggapan']))

{

$model->attributes=$_POST['Tanggapan'];

if($model->save())

$this->redirect(array('view','id'=>$model->id));

}



$this->render('update',array(

'model'=>$model,

));

}



/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/

public function actionDelete($id)

{

if(Yii::app()->request->isPostRequest)

{

// we only allow deletion via POST request

$this->loadModel($id)->delete();



// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

if(!isset($_GET['ajax']))

$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

}

else

throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

}



/**
* Lists all models.
*/

public function actionIndex()

{

$dataProvider=new CActiveDataProvider('Tanggapan');

$this->render('index',array(

'dataProvider'=>$dataProvider,

));

}



/**
* Manages all models.
*/

public function actionAdmin()

{

$model=new Tanggapan('search');

$model->unsetAttributes();  // clear any default values

if(isset($_GET['Tanggapan']))

$model->attributes=$_GET['Tanggapan'];



$this->render('admin',array(

'model'=>$model,

));

}



/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loade
*/

public function loadModel($id)

{

$model=Tanggapan::model()->findByPk($id);

if($model===null)

throw new CHttpException(404,'The requested page does not exist.');

return $model;

}



/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/

protected function performAjaxValidation($model)

{

if(isset($_POST['ajax']) && $_POST['ajax']==='tanggapan-form')

{

echo CActiveForm::validate($model);

Yii::app()->end();

}

}

}

