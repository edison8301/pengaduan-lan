<?php

class UnitController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/main';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','migrasi','changePassword','setPassword'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'User::isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
	$this->render('view',array(
	'model'=>$this->loadModel($id),
	));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Unit;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];
			$model->password = CPasswordHelper::hashPassword($model->password);

			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Unit']))
		{
			$model->attributes=$_POST['Unit'];

			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{

		$criteria = new CDbCriteria;
		$params = array();

		$unit = Unit::getId();

		$criteria->condition = 'id_unit = :unit';
		$criteria->order = 'waktu_dibuat DESC';
		$params[':unit']=$unit;

		$criteria->params=$params;

		$dataProvider=new CActiveDataProvider('Pengaduan',array(
			'criteria'=>$criteria
		));
				
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionChangePassword()
	{
		$model = Unit::model()->findByAttributes(array('username'=>Yii::app()->user->id));

		$ChangePasswordUnitForm = new ChangePasswordUnitForm;

		if(isset($_POST['ChangePasswordUnitForm']))
		{
			$ChangePasswordUnitForm->attributes = $_POST['ChangePasswordUnitForm'];

			if($ChangePasswordUnitForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($ChangePasswordUnitForm->password_baru);
				$model->save();
				Yii::app()->user->setFlash('success','Password berhasil diperbarui');
				$this->redirect(array('unit/changePassword'));
			}
		}

		$this->render('changePassword',array(
			'model'=>$model,
			'ChangePasswordUnitForm'=>$ChangePasswordUnitForm
		));
	}

	public function actionSetPassword($id)
	{
		$model = $this->loadModel($id);

		$SetPasswordForm = new SetPasswordForm;

		if(isset($_POST['SetPasswordForm']))
		{
			$SetPasswordForm->attributes = $_POST['SetPasswordForm'];

			if($SetPasswordForm->validate())
			{
				$model->password = CPasswordHelper::hashPassword($SetPasswordForm->password_baru);
				$model->save();
				Yii::app()->user->setFlash('success','Password berhasil diperbarui');
				$this->redirect(array('unit/admin'));
			}
		}

		$this->render('setPassword',array(
			'model'=>$model,
			'SetPasswordForm'=>$SetPasswordForm
		));
	}	

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Unit('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Unit']))
$model->attributes=$_GET['Unit'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Unit::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='unit-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
