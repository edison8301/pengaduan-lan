<?php



class SiteController extends Controller

{

	/**
	 * Declares class-based actions.
	 */

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),

			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName

			'page'=>array(
				'class'=>'CViewAction',
			),

		);

	}



	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

	public function actionIndex()
	{
		$this->layout = '//layouts/column2';

		/*mail ke pengadu*/

		$model = new Pengaduan;

		$model->scenario = 'create';

		$this->performAjaxValidation($model);

		if(isset($_POST['Pengaduan']))

		{
			/*data input pengadu*/

			$model->attributes = $_POST['Pengaduan'];
			$model->id_status = 1;
			$model->kode = Helper::getKode();

			//date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');

			/*ambil email unit*/

		
			if($model->save())
			{
				$model->sendMailToComplainer();
				$model->sendMailToUnit();

				Yii::app()->user->setFlash('success',' <strong>Berhasil !</strong> Pengaduan Anda sudah telah berhasil dikirim. Kami akan segera menanggapi dan melakukan tindakan terhadap pengaduan anda');

				$this->redirect(array('site/index'));
			}
      	


		}


		$this->render('index',array(

			'model'=>$model

		));

	}

	public function actionAlurPengaduan()
	{
		$this->layout = '//layouts/column2';
		
		$this->render('alur_pengaduan'); 
	}



	public function actionCekTiket()
	{
		$this->layout = '//layouts/column2';

		if(isset($_POST['tanggapan']))
		{
			$kode = $_GET['kode'];
			$pengaduan = Pengaduan::model()->findByAttributes(array('kode'=>$kode));

			$model = new Tanggapan;
			$model->id_pengaduan = $pengaduan->id;
			
			$model->username_pembuat = $pengaduan->kode;
			$model->tanggapan = $_POST['tanggapan'];

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s'); 

			if($model->save())
			{
				$model->sendMailToUnit();
				$this->redirect(array('site/cekTiket','kode'=>$_GET['kode']));
			}

		}

		$this->render('cektiket',array());
	}



	public function actionTanggapanPengguna()
	{

		$model=new Tanggapan;

		$this->layout = '//layouts/column2';

		if(isset($_GET['id_pengaduan']))
		{
			$model->id_pengaduan = $_GET['id_pengaduan'];
			
			$model->username_pembuat = $_GET['pengirim'];
			$model->tanggapan = $_GET['tanggapan'];

			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s'); 

			if($model->save())
			{
				$model->sendMailToUnit();
				$this->redirect(array('site/cekTiket','kode'=>$_GET['kode']));
			}

		}

		$this->layout = '//layouts/column2';
		$this->render('cektiket',array(
			'model'=>$model
		));
	}




	/**
	 * This is the action to handle external exceptions.
	 */

	public function actionError()

	{

		if($error=Yii::app()->errorHandler->error)

		{

			if(Yii::app()->request->isAjaxRequest)

				echo $error['message'];

			else

				$this->render('error', $error);

		}

	}



	/**
	 * Displays the contact page
	 */

	public function actionContact()

	{

		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				//use 'contact' view from views/mail
				$mail = new YiiMailer();
				
				//set properties
				$mail->setFrom($model->email, $model->name);
				$mail->setSubject($model->subject);
				$mail->setTo(Yii::app()->params['adminEmail']);
				//send
				if ($mail->send()) {
					Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				} else {
					Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
				}
				
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));

	}

	public function actionKirimEmail()
	{
			$mail = new YiiMailer();
			
			$smtp_server = "previewaplikasi.com";
			$smtp_port = "465";
			$email = "dadansatria@previewaplikasi.com";
			$password = "prevburger123";
			$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
			
			$mail->setFrom($email,'Dadan Satria Nugraha');
			$mail->setBody('Simple message');
				
			$mail->setSubject("Oke sipp: lorem lorem ");

				$mail->setTo('dadansatria7@gmail.com');
				if($mail->send())
					print 'oke';
				else
					print $mail->getError();
				
	}


	/**
	 * Displays the login page
	 */

	public function actionLogin()

	{

		$this->layout='//layouts/login';

		$model=new LoginForm;

		if(!Yii::app()->user->isGuest)
			$this->redirect(array('admin/index'));

		// if it is ajax validation request

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')

		{

			echo CActiveForm::validate($model);

			Yii::app()->end();

		}



		// collect user input data

		if(isset($_POST['LoginForm']))

		{

			$model->attributes=$_POST['LoginForm'];

			// validate user input and redirect to the previous page if valid

			if($model->validate() && $model->login())

				$this->redirect(array('admin/index'));;

		}

		// display the login form

		$this->render('login',array('model'=>$model));

	}



	/**
	 * Logs out the current user and redirect to homepage.
	 */

	public function actionLogout()

	{

		Yii::app()->user->logout();

		$this->redirect(Yii::app()->homeUrl);

	}



	protected function performAjaxValidation($model)

    {

        if(isset($_POST['ajax']))

        {

            echo CActiveForm::validate($model);

            Yii::app()->end();

        }

    }





}