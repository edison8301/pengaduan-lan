<?php

class AdminController extends Controller
{

	public $layout = '//layouts/admin/dashboard';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		

		$criteria = new CDbCriteria;
		$params = array();

		if(User::isUnit())
		{
			$criteria->with = array('tanggapan');
			$criteria->together = true;

			$criteria->addCondition('t.id_unit = :id_unit OR tanggapan.id_unit_tujuan = :id_unit');
			$params[':id_unit'] = User::getIdUnitByUserId();
		}

		if(User::isDeputi())
		{
			//$data = DeputiUnit::model()->findAllByAttributes(array('id_deputi'=>Deputi::getIdDeputiByUserId()));			
			$criteria->addCondition('t.id_unit IN (SELECT id_unit FROM deputi_unit WHERE id_deputi = :id_deputi)');
			$params[':id_deputi'] = Deputi::getIdDeputiByUserId();
		}

		$criteria->params = $params;

		$criteria->order = 't.waktu_dibuat DESC';

		$dataProvider=new CActiveDataProvider('Pengaduan',array(
			'criteria'=>$criteria,
			'pagination'=>array(
      		  'pageSize'=>10,
    		),
		));
			
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionGrafikBulan()
	{
		
		$this->render('grafikperbulan');

	}

	public function actionGrafikKategori()
	{
		
		$this->render('grafikperkategori');

	}

}