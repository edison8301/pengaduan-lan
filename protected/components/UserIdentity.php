<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{

		$unit=Unit::model()->findByAttributes(array('username'=>$this->username));
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		$deputi=Deputi::model()->findByAttributes(array('username'=>$this->username));
		
		if($user!==null) 
		{
			if(CPasswordHelper::verifyPassword($this->password,$user->password))
			{	

				$this->errorCode=self::ERROR_NONE;		
				
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}

		} elseif($unit!==null) {
			if(CPasswordHelper::verifyPassword($this->password,$unit->password))
			{	
				$this->errorCode=self::ERROR_NONE;		
				
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}

		} elseif($deputi!==null) {
			if(CPasswordHelper::verifyPassword($this->password,$deputi->password))
			{	
				$this->errorCode=self::ERROR_NONE;		
				
			} else {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			}

		} else {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}


		return !$this->errorCode;
	}
}

//<?php print CPasswordHelper::hashPassword("burger123");