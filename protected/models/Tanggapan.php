<?php

/**
 * This is the model class for table "tanggapan".
 *
 * The followings are the available columns in table 'tanggapan':
 * @property integer $id
 * @property integer $id_pengaduan
 * @property string $tanggapan
 * @property integer $pengirim
 * @property string $waktu_dibuat
 */
class Tanggapan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tanggapan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_unit_tujuan','required','on'=>'disposisi'),
			array('id_pengaduan, tanggapan', 'required'),
			array('id_pengaduan, id_unit_tujuan', 'numerical', 'integerOnly'=>true),
			array('waktu_dibuat, waktu_dilihat','safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_pengaduan, tanggapan, pengirim, waktu_dibuat, waktu_dilihat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pengaduan' => 'Id Pengaduan',
			'tanggapan' => 'Tanggapan',
			'pengirim' => 'Pengirim',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_dibuat' => 'Waktu Dibuat',
			'id_unit_tujuan'=>'Tujuan Disposisi'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pengaduan',$this->id_pengaduan);
		$criteria->compare('tanggapan',$this->tanggapan,true);
		$criteria->compare('pengirim',$this->pengirim);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tanggapan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getPengaduan()
	{
		$model = Pengaduan::model()->findByPk($this->id_pengaduan);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function sendMailToComplainer()
	{
		$mail = new YiiMailer();

		$pengadu = Pengaduan::model()->findByAttributes(array('id'=>$this->id_pengaduan));
			
		$smtp_server = "previewaplikasi.com";
		$smtp_port = "465";
		$email = "dadansatria@previewaplikasi.com";
		$password = "prevburger123";
		$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
		
		$mail->setSubject("Tanggapan Telah Kami Terima");

		$mail->setFrom($email,'Pengaduan LAN');
		$mail->setBody('Pengaduan LAN');

		$mail->MsgHTML('<html>
        <head>
        </head>
        <body>
            Dear <b>'.$pengadu->nama.' </b>
            <br><br>
            Kami Telah Menanggapi perihal pengaduan anda dengan data sebagai berikut : 
            <br><br>
            <table>
            	<tr>
            		<td>Nama</td>
            		<td>:</td>
            		<td>'.$pengadu->nama.'</td>
            	</tr>
            	<tr>
            		<td>E-Mail</td>
            		<td>:</td>
            		<td>'.$pengadu->email.'</td>
            	</tr> 
            	<tr>
            		<td>Telepon</td>
            		<td>:</td>
            		<td>'.$pengadu->telepon.'</td>
            	</tr> 
            	<tr>
            		<td>Dengan Pengaduan</td>
            		<td>:</td>
            		<td>'.$pengadu->keluhan.'</td>
            	</tr> 
            	<tr>
            		<td>Kode Pengaduan</td>
            		<td>:</td>
            		<td><b>'.$pengadu->kode.'</b></td>
            	</tr>                         	            	           
            </table>

            <br><br>
            Pada Hari<b> '.Helper::getCreatedTime($this->waktu_dibuat).' </b> Dengan Rincian Tanggapan Sebagai Berikut
            <br><br>
            <table>
            	<tr>
            		<td>Tanggapan</td>
            		<td>:</td>
            		<td><b>'.$this->tanggapan.'</b></td>
            	</tr>                 	
            </table>
                <br>anda dapat melakukan cek pengaduan di link berikut 
                http://pengaduan.lan.go.id/baru/index.php?r=site/cekTiket dengan menginput kode
                pengaduan sebagai berikut <b>'.$pengadu->kode.'</b><br><br>
                Terima Kasih atas pengaduannya
        </body>
		</html>');

		$mail->setTo($pengadu->email);
		return $mail->send();

	}

	public function sendMailToUnit()
	{
		$mail = new YiiMailer();

		$pengadu = Pengaduan::model()->findByAttributes(array('id'=>$this->id_pengaduan));
		$unit = Unit::model()->findByAttributes(array('id'=>$pengadu->id_unit));
			
		$smtp_server = "previewaplikasi.com";
		$smtp_port = "465";
		$email = "dadansatria@previewaplikasi.com";
		$password = "prevburger123";
		$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
		
		$mail->setSubject("Balasan Dari Pengadu ".$pengadu->nama);

		$mail->setFrom($email,'Pengaduan LAN');
		$mail->setBody('Pengaduan LAN');

		$mail->MsgHTML('	<html>
        <head>
        </head>
        <body>
            Yang Terhormat <b>'.$pengadu->nama.'</b>
            <br><br>
            Pengguna dengan data sebagai berikut : 
            <br><br>
            <table>
            	<tr>
            		<td>Nama</td>
            		<td>:</td>
            		<td>'.$pengadu->nama.'</td>
            	</tr>
            	<tr>
            		<td>E-Mail</td>
            		<td>:</td>
            		<td>'.$pengadu->email.'</td>
            	</tr> 
            	<tr>
            		<td>Telepon</td>
            		<td>:</td>
            		<td>'.$pengadu->telepon.'</td>
            	</tr> 
            	<tr>
            		<td>Dengan Pengaduan</td>
            		<td>:</td>
            		<td>'.$pengadu->keluhan.'</td>
            	</tr> 
            	<tr>
            		<td>Kode Pengaduan</td>
            		<td>:</td>
            		<td><b>'.$pengadu->kode.'</b></td>
            	</tr>                         	            	           
            </table>

            <br><br>
            Telah membalas taggapan anda pada Hari<b> '.Helper::getCreatedTime($this->waktu_dibuat).' </b> dengan rincian sebagai berikut :
            <br><br>

            '.$this->tanggapan.'

                <br><br>Untuk membalas,
               <a href="http://pengaduan.lan.go.id/baru/index.php?r=tanggapan/create&id_pengaduan='.$pengadu->id.' ">
               klik disini
               </a>
        </body>
		</html>');

		$mail->setTo($unit->email);
		return $mail->send();

	}

	public function getUnitTujuan()
	{
		
		$model = Unit::model()->findByPk($this->id_unit_tujuan);

		if($model!==null)
		{
			return $model->nama;
		} else {
			return null;
		}
	}

	public function getPembuat()
	{
		$pengaduan = Pengaduan::model()->findByAttributes(array('kode'=>$this->username_pembuat));
		
		if($pengaduan!==null)
		{
			return $pengaduan->nama;
		}

		$unit = Unit::model()->findByAttributes(array('username'=>$this->username_pembuat));
		
		if($unit!==null)
		{
			return $unit->nama;
		}

		$user = User::model()->findByAttributes(array('username'=>$this->username_pembuat));

		if($user!==null)
		{
			return "ADMIN";
		}

		return null;
	}



}
