<?php



/**
 * This is the model class for table "pengaduan".
 *
 * The followings are the available columns in table 'pengaduan':
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property string $email
 * @property string $telepon
 * @property integer $id_kategori
 * @property string $keluhan
 * @property integer $id_status
 * @property string $waktu_dibuat
 */

class Pengaduan extends CActiveRecord

{

	/*
	 * @return string the associated database table nam
	 */

	public function tableName()
	{
		return 'pengaduan';
	}

	public $keamanan;
	public $setuju;



	/*
	 * @return array validation rules for model attributes
	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(
			array('kode, nama, email, telepon, id_unit, keluhan, id_status', 'required','message'=>'{attribute} tidak boleh kosong'),
			array('id_kategori, id_status', 'numerical', 'integerOnly'=>true),
			array('kode, nama, email, telepon', 'length', 'max'=>255),
			array('id_unit','required','message'=>'{attribute} Tidak Boleh Kosong'),
			array('setuju','required','message'=>'Anda Harus menyatakan bahwa yang anda tulis adalah benar!','on'=>'create'),
			array('email','email','message'=>'Alamat email anda tidak benar'),
			array('waktu_dibuat', 'safe'),
			array('keamanan', 'captcha', 'allowEmpty'=>CCaptcha::checkRequirements(),'message'=>'Kode Chapcha Tidak Benar !'),

			// The following rule is used by search().

			// @todo Please remove those attributes that should not be searched.

			array('id, kode, nama, email, telepon, id_kategori, keluhan, id_status, waktu_dibuat', 'safe', 'on'=>'search'),

		);

	}



	/*
	 * @return array relational rules
	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(
			'disposisi'=>array(self::HAS_MANY,'Disposisi','id_pengaduan'),
			'tanggapan'=>array(self::HAS_MANY,'Tanggapan','id_pengaduan'),
			'deputiunit'=>array(self::HAS_MANY,'DeputiUnit','id_unit'),			
		);

	}



	/*
	 * @return array customized attribute labels (name=>label
	 */

	public function attributeLabels()

	{

		return array(

			'id' => 'ID',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'email' => 'Email',
			'telepon' => 'Telepon',
			'id_kategori' => 'Kategori',
			'keluhan' => 'Keluhan',
			'id_status' => 'Id Status',
			'waktu_dibuat' => 'Waktu Dibuat',
			'id_unit' => 'Ditujukan',

		);

	}



	/*
	 * Retrieves a list of models based on the current search/filter conditions
	 
	 * Typical usecase
	 * - Initialize the model fields with values from filter form
	 * - Execute this method to get CActiveDataProvider instance which will filte
	 * models according to data in model fields
	 * - Pass data provider to CGridView, CListView or any similar widget
	 
	 * @return CActiveDataProvider the data provider that can return the model
	 * based on the search/filter conditions
	 */

	public function search()
	{

		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('keluhan',$this->keluhan,true);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);
		$criteria->order = 'waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

	}


	public function pengaduanUnit()

	{

		// @todo Please modify the following code to remove attributes that should not be searched.


		$criteria = new CDbCriteria;
		$criteria->with = array('tanggapan');
		$criteria->together = true;

		$criteria->addCondition('id_unit = :id_unit OR tanggapan.id_unit_tujuan = :id_unit');
		$criteria->params = array(':id_unit'=>User::getIdUnitByUserId());

		$criteria->compare('id',$this->id);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('id_kategori',$this->id_kategori);
		$criteria->compare('keluhan',$this->keluhan,true);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		$criteria->order = 't.waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

		));

	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pengaduan the static model class
	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	public function getKategori()

	{

		$model = Kategori::model()->findByPk($this->id_kategori);

		if ($model!==null)

			return $model->nama;

		else

			return null;

	}



		public function getStatus()

	{

		$model = Status::model()->findByPk($this->id_status);

		if ($model!==null)

			return $model->nama;

		else

			return null;

	}

	public static function countSemua()
	{

		return Pengaduan::model()->count();

	}

	public static function countHariIni()
	{
		$criteria = new CDbCriteria;
		$params = array();

		date_default_timezone_set('Asia/Jakarta');
		

		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$params[':awal'] = date('Y-m-d').' 00:00:00';
		$params[':akhir'] = date('Y-m-d').' 23:59:59';

		$criteria->params = $params;

		return Pengaduan::model()->count($criteria);

	}

	public static function countBulanIni()
	{
		$criteria = new CDbCriteria;
		$params = array();

		date_default_timezone_set('Asia/Jakarta');
		$tanggal = date('Y-m-d',strtotime('monday this week'));

		$criteria->addCondition('waktu_dibuat >= :awal AND waktu_dibuat <= :akhir');
		$params[':awal'] = date('Y-m').'-01 00:00:00';
		$params[':akhir'] = date('Y-m').'-31 23:59:59';

		$criteria->params = $params;

		return Pengaduan::model()->count($criteria);

	}

	public function findAllTanggapan()

	{

		$model = Tanggapan::model()->findAllByAttributes(array('id_pengaduan'=>$this->id));

		

		if($model!==null)

			return $model;

		else

			return false;

	}

	public function findAllDisposisi()
	{

		$model = Disposisi::model()->findAllByAttributes(array('id_pengaduan'=>$this->id));

		if($model!==null)
			return $model;
		else
			return false;

	}

	public function findAllPengaduan() 
	{

		$model = $this->findAll();

		if($model!==null)

			return $model;

		else

			return false;

	}



	public static function getDataChartBulan()

	{

		$dataChartBulan = '';



		for($i=1;$i<=12;$i++)

		{

   		 $criteria = new CDbCriteria;

    

    		$bulan = $i;



    		if($i<=10) $bulan = '0'.$i;



   			$awal = date('Y').'-'.$bulan.'-01';

    		$akhir = date('Y').'-'.$bulan.'-31';

    

	    $criteria->condition = 'waktu_dibuat >= :awal AND waktu_dibuat <= :akhir';

	    $criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);

    

	    $jumlah_acara = Pengaduan::model()->count($criteria);



	    $nama_bulan = '';

	    if($i==1) $nama_bulan = 'Jan';

	    if($i==2) $nama_bulan = 'Feb';

	    if($i==3) $nama_bulan = 'Mar';

	    if($i==4) $nama_bulan = 'Apr';

	    if($i==5) $nama_bulan = 'Mei';

	    if($i==6) $nama_bulan = 'Jun';

	    if($i==7) $nama_bulan = 'Jul';

	    if($i==8) $nama_bulan = 'Aug';

	    if($i==9) $nama_bulan = 'Sep';

	    if($i==10) $nama_bulan = 'Okt';

	    if($i==11) $nama_bulan = 'Nov';

	    if($i==12) $nama_bulan = 'Des';





    

    $dataChartBulan .= '{"label":"'.$nama_bulan.'","value":"'.$jumlah_acara.'"},';

	}

		return $dataChartBulan;

	}





	public static function getdataChartUnit()

	{

		$dataChartJenis = '';


 		foreach(Unit::model()->findAll() as $data) 

  		{ 

			$dataChartJenis .= '{"label":"'.$data->nama.'","value":"'.$data->getCountData().'"},';

  		}

  			return $dataChartJenis;

	}

	public function sendMailToComplainer()
	{
		$mail = new YiiMailer();
			
		$smtp_server = "previewaplikasi.com";
		$smtp_port = "465";
		$email = "dadansatria@previewaplikasi.com";
		$password = "prevburger123";
		$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
		
		$mail->setSubject("Pengaduan Telah Kami Terima");

		$mail->setFrom($email,'Pengaduan LAN');
		$mail->setBody('Pengaduan LAN');

		$mail->MsgHTML('	<html>
        <head>
        </head>
        <body>
            Dear <b>'.$this->nama.' </b>
            <br><br>
            Kami Telah menerima pengaduan anda dengan data sebagai berikut : 
            <br><br>
            <table>
            	<tr>
            		<td>Nama</td>
            		<td>:</td>
            		<td>'.$this->nama.'</td>
            	</tr>
            	<tr>
            		<td>E-Mail</td>
            		<td>:</td>
            		<td>'.$this->email.'</td>
            	</tr> 
            	<tr>
            		<td>Telepon</td>
            		<td>:</td>
            		<td>'.$this->telepon.'</td>
            	</tr> 
            	<tr>
            		<td>Kode Pengaduan</td>
            		<td>:</td>
            		<td><b>'.$this->kode.'</b></td>
            	</tr> 
            	<tr>
            		<td>Dengan Pengaduan</td>
            		<td>:</td>
            		<td>'.$this->keluhan.'</td>
            	</tr> 
            </table>
          
                <br>Kami akan segera menanggapi pengaduan anda. anda dapat melakukan cek pengaduan di link berikut 
                http://pengaduan.lan.go.id/baru/index.php?r=site/cekTiket dengan menginput kode pengaduan
                berikut <b>'.$this->kode.'</b><br><br>
                Terima Kasih atas pengaduannya
        </body>
       </html>');

		$mail->setTo($this->email);
		return $mail->send();

	}

	public function sendMailToUnit()
	{
		$mail = new YiiMailer();
			
		$smtp_server = "previewaplikasi.com";
		$smtp_port = "465";
		$email = "dadansatria@previewaplikasi.com";
		$password = "prevburger123";
		$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
		
		$mail->setSubject("Pengaduan Baru Dari ".$this->nama);

		$mail->setFrom($email,'Pengaduan LAN');
		$mail->setBody('Pengaduan LAN');

		$unit = Unit::model()->findByAttributes(array('id'=>$this->id_unit));

		$mail->MsgHTML('	<html>
        <head>
        </head>
        <body>
            Dear <b>'.$this->nama.' </b>
            <br><br>
            Pengadu dengan data sebagai berikut : 
            <br><br>
            <table>
            	<tr>
            		<td>Nama</td>
            		<td>:</td>
            		<td>'.$this->nama.'</td>
            	</tr>
            	<tr>
            		<td>E-Mail</td>
            		<td>:</td>
            		<td>'.$this->email.'</td>
            	</tr> 
            	<tr>
            		<td>Telepon</td>
            		<td>:</td>
            		<td>'.$this->telepon.'</td>
            	</tr> 
            	<tr>
            		<td>Kode Pengaduan</td>
            		<td>:</td>
            		<td><b>'.$this->kode.'</b></td>
            	</tr>  
            	<tr>
            		<td>Dengan Pengaduan</td>
            		<td>:</td>
            		<td>'.$this->keluhan.'</td>
            	</tr>                        	            	           
            </table>
          
                <br>Telah mengirimkan pengaduan via SIAP LAN. Untuk menamggapi pengaduan tersebut, klik link berikut
                http://pengaduan.lan.go.id/baru/index.php?r=pengaduan/view&id='.$this->id.'. Mohon untuk segera ditanggapi.
        </body>
       </html>');

		$mail->setTo($unit->email);
		return $mail->send();

	}

	public function getCekPengaduanDisposisi()
	{
		$model = Disposisi::model()->findByAttributes(array('id_tiket'=>$this->id));
		if($model !==null)
			return "Disposisi Telah Dikirim";
		else
			return CHtml::link("Kirim Disposisi",array("disposisi/create","id_pengaduan"=>$this->id),array("class"=>"btn btn-primary btn-xs"));
	}

	public static function getCekPengaduanDisposisiView($id)
	{
		$model = Disposisi::model()->findByAttributes(array('id_pengaduan'=>$id));
		if($model !==null)
			return $model->id;
		else
			return null;
	}

	public static function countDisposisiBaru()
	{

		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_unit_tujuan = :id_unit_tujuan AND waktu_dilihat IS NULL');
		
		$params[':id_unit_tujuan'] = Unit::getId();

		$criteria->params = $params;

		return Disposisi::model()->count($criteria);
		
	}

	public static function countPengaduanBaru()
	{
		$criteria = new CDbCriteria;
		$params = array();

		$criteria->addCondition('id_unit = :id_unit AND waktu_dilihat IS NULL');
		
		$params[':id_unit'] = Unit::getId();

		$criteria->params = $params;

		return Pengaduan::model()->count($criteria);

	}

	public function getCssClass($data)
	{
		$cssClass;

	    if($data ==null)
	    {
	      return $cssClass='success';
	    }
	    else
	    {
	    	return $cssClass = null;
	    }
	 }

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getUnit()
	{
	
		if($this->id_unit==null)
		{
			return "ADMIN";
		}
		
		$model = Unit::model()->findByPk($this->id_unit);

		if($model!==null)
		{
			return $model->nama;
		} else {
			return null;
		}
	}

}

