<?php

/**
 * This is the model class for table "disposisi".
 *
 * The followings are the available columns in table 'disposisi':
 * @property integer $id
 * @property integer $id_tiket
 * @property string $no_tiket
 * @property integer $id_unit
 * @property string $catatan
 * @property string $tanggal
 */
class Disposisi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'disposisi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_unit_tujuan', 'required'),
			array('id_pengaduan, id_unit_tujuan, id_unit_asal', 'numerical', 'integerOnly'=>true),
			array('catatan','safe'),
			array('id, id_tiket, no_tiket, id_unit, catatan, tanggal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pengaduan'=>array(self::BELONGS_TO,'Pengaduan','id_unit'),
			'unit_asal'=>array(self::BELONGS_TO,'Unit','id_unit_asal'),
			'unit_tujuan'=>array(self::BELONGS_TO,'Unit','id_unit_tujuan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_tiket' => 'Tiket',
			'no_tiket' => 'No Tiket',
			'id_unit_asal' => 'Unit Asal',
			'id_unit_tujuan' => 'Unit Tujuan',
			'catatan' => 'Catatan',
			'tanggal' => 'Tanggal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;


		$criteria->compare('id',$this->id);
		$criteria->compare('id_pengaduan',$this->id_pengaduan);
		$criteria->compare('id_unit',$this->id_unit);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('tanggal',$this->tanggal,true);

		$criteria->order = 'tanggal DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function disposisiUnit()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$unit = Unit::getId();

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pengaduan',$this->id_pengaduan);
		$criteria->compare('id_unit_tujuan',$unit,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('tanggal',$this->tanggal,true);

		$criteria->order = 'tanggal DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Disposisi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getKodeTiket()
	{
		$model = Pengaduan::model()->findByAttributes(array('id'=>$this->id_tiket));
		if($model !==null)
			return $model->kode;
		else
			return null;
	}

	public function getTujuan()
	{
		$model = Unit::model()->findByAttributes(array('id'=>$this->id_unit_tujuan));
		if($model !==null)
			return $model->nama;
		else
			return null;
	}

	public function getCssClass($data)
	{
		$cssClass;

	    if($data ==null)
	    {
	      return $cssClass='success';
	    }
	    else
	    {
	    	return $cssClass = null;
	    }
	}

	public function findPengaduan()
	{
		return 	Pengaduan::model()->findByPk($this->id_pengaduan);
	}

	public function sendMailToUnit()
	{
		$mail = new YiiMailer();
			
		$smtp_server = "previewaplikasi.com";
		$smtp_port = "465";
		$email = "dadansatria@previewaplikasi.com";
		$password = "prevburger123";

		$unit = Unit::model()->findByAttributes(array('id'=>$this->id_unit_tujuan));
		$pengaduan = $this->findPengaduan();

		$mail->setSmtp($smtp_server, $smtp_port, 'ssl', true, $email,$password);
		
		$mail->setSubject("Disposisi Baru Dari Unit ".$unit->nama);

		$mail->setFrom($email,'Pengaduan LAN');
		$mail->setBody('Pengaduan LAN');

		$mail->MsgHTML('	<html>
        <head>
        </head>
        <body>
            Dengan Hormat, Unit <b>'.$unit->nama.' </b>
            <br><br>
            Kami telah mendisposisikan pengaduan dengan detail sebagai berikut:
            <br><br>
            <table>
            	<tr>
            		<td>Nama</td>
            		<td>:</td>
            		<td>'.$pengaduan->nama.'</td>
            	</tr>
            	<tr>
            		<td>E-Mail</td>
            		<td>:</td>
            		<td>'.$pengaduan->email.'</td>
            	</tr> 
            	<tr>
            		<td>Telepon</td>
            		<td>:</td>
            		<td>'.$pengaduan->telepon.'</td>
            	</tr> 
            	<tr>
            		<td>Kode Pengaduan</td>
            		<td>:</td>
            		<td><b>'.$pengaduan->kode.'</b></td>
            	</tr>  
            	<tr>
            		<td>Dengan Pengaduan</td>
            		<td>:</td>
            		<td>'.$pengaduan->keluhan.'</td>
            	</tr>
            </table>
          
                <br>Telah mengirimkan pengaduan via SIAP LAN. Untuk menamggapi pengaduan tersebut, klik link berikut
                http://pengaduan.lan.go.id/baru/index.php?r=pengaduan/view&id='.$pengaduan->id.'. Mohon untuk segera ditanggapi.
        </body>
       </html>');

		$mail->setTo($unit->email);
		return $mail->send();

	}

	public function getRelation($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function getUnitAsal()
	{
		if($this->id_unit_asal==null)
		{
			return "Admin";
		}
	}

	public function getUnitTujuan()
	{
		
		$model = Unit::model()->findByPk($this->id_unit_tujuan);

		if($model!==null)
		{
			return $model->nama;
		} else {
			return null;
		}
	}




}
