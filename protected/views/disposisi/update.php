<?php
$this->breadcrumbs=array(
	'Disposisi'=>array('admin'),
	'Sunting',
);

	$this->menu=array(
	array('label'=>'List Disposisi','url'=>array('index')),
	array('label'=>'Create Disposisi','url'=>array('create')),
	array('label'=>'View Disposisi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Disposisi','url'=>array('admin')),
	);
	?>

	<h1>Sunting Disposisi </h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>