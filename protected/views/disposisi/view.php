<?php
$this->breadcrumbs=array(
	'Disposisi'=>array('admin'),
	'Detail Disposisi'
);

$this->menu=array(
array('label'=>'List Disposisi','url'=>array('index')),
array('label'=>'Create Disposisi','url'=>array('create')),
array('label'=>'Update Disposisi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Disposisi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Disposisi','url'=>array('admin')),
);
?>

<h1>Detail Disposisi</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type' => 'striped bordered condensed',
	'attributes'=>array(
			array(
				'name' => 'no_tiket',
				'value' => $model->getKodeTiket()),
			array(
				'name' => 'id_unit',
				'value' => $model->getTujuan()),
			'catatan',
			array(
				'name' => 'tanggal',
				'value' => Helper::tanggal($model->tanggal))
	),
)); ?>

<hr>

<?php $pengaduan = Pengaduan::model()->findByAttributes(array('id'=>$model->id_tiket)); ?>

<h2>Detail Pengaduan</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tanggapi',
		'icon'=>'envelope',
		'context'=>'primary',
		'url'=>array('tanggapan/create','id_pengaduan'=>$pengaduan->id)
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Disposisikan',
		'icon'=>'share-alt',
		'context'=>'primary',
		'url'=>array('disposisi/create','id_pengaduan'=>$pengaduan->id)
)); ?>&nbsp;

<div>&nbsp;</div>
<table width="100%" class="table table-hover table-striped table-bordered">
	<tr>
		<td style="padding-top: 30px; padding-left: 80px; padding-right: 0px;" rowspan="8"><img class="media-object" style="width: 130px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/envelope_open.png"></td>
	</tr>
	<tr>
		<td>Kode</td>
		<td>:</td>
		<td style="font-weight: bold"><?php print $pengaduan->kode; ?></td>
	</tr>
	<tr>
		<td>Pengadu</td>
		<td>:</td>
		<td style="font-weight: bold"><?php print $pengaduan->nama; ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td style="font-weight: bold"><?php print $pengaduan->email; ?></td>
	</tr>
	<tr>
		<td>Telepon</td>
		<td>:</td>
		<td style="font-weight: bold"><?php print $pengaduan->telepon; ?></td>
	</tr>
	<tr>
		<td>Keluhan</td>
		<td>:</td>
		<td style="font-weight: bold; max-width: 100px; "><?php print $pengaduan->keluhan; ?></td>
	</tr>
	<tr>
		<td>Status</td>
		<td>:</td>
		<td style="font-weight: bold"><span class="label label-prymary"><?php print $pengaduan->getStatus(); ?></span></td>
	</tr>
	<tr>
		<td>Waktu Pengaduan</td>
		<td>:</td>
		<td style="font-weight: bold"><?php print Helper::getCreatedTime($pengaduan->kode); ?></td>
	</tr>

</table>

