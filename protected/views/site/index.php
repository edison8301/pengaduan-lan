<div id="inner">

<h4>Selamat Datang di SIAP Lembaga Administrasi Negara</h4>

<hr>

    <h5 style="font-weight:bold">Silakan masukkan data aspirasi dan pengaduan anda</h5>

    <div>&nbsp;</div>

    <div id="formdepan">

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm',array(
                'id' => 'verticalForm',
           	    'type' => 'horizontal',
           	    'enableAjaxValidation'=>true
    )); ?>
        
        <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'nama',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
            'widgetOptions' => array('rows'=>6, 'cols'=>50, 'class'=>'span8'),'prepend'=>'<i class="glyphicon glyphicon-share-alt"></i>')); ?>  

    <?php echo $form->textFieldGroup($model,'email',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
            'widgetOptions' => array('rows'=>6, 'cols'=>50, 'class'=>'span8'),'prepend'=>'<i class="glyphicon glyphicon-share-alt"></i>')); ?>  

    <?php echo $form->numberFieldGroup($model,'telepon',array(
            'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
            'widgetOptions' => array('rows'=>6, 'cols'=>50, 'class'=>'span8'),'prepend'=>'<i class="glyphicon glyphicon-share-alt"></i>')); ?>  

    <?php echo $form->select2Group($model,'id_unit',array(
         'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
            'widgetOptions'=>array(
                'data' => CHtml::ListData(Unit::model()->findAll(), 'id', 'nama'),
                'htmlOptions'=>array('empty'=>'-- Pilih Tujuan --')
            )
    ));  ?>


    <?php echo $form->textAreaGroup($model, 'keluhan',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('rows'=>'3')
            ),
    )); ?>

    <?php echo $form->fileFieldGroup($model,'file',array(
        'wrapperHtmlOptions'=>array('class'=>'col-sm-8'),
            'widgetOptions'=>array(
                'data' => CHtml::ListData(Kategori::model()->findAll(), 'id', 'nama'),
                'htmlOptions'=>array('empty'=>'-- Pilih Kategori --'),

            ),
            'hint' => 'Format File .jpg .gif .png .doc .docx .xls .xlsx .ppt .pptx .pdf'
    ));  ?>

    <div class="col-sm-2">
        <?php echo $form->labelEx($model,'Kode Konfirmasi'); ?>
    </div>
    <div class="col-sm-10">
        <?php $this->widget('CCaptcha'); ?>
        <?php echo $form->textField($model,'keamanan',array()); ?>
    </div>

    <div class="col-sm-12">
        <?php echo $form->checkbox($model, 'setuju'); ?> Saya yang Menyatakan pengaduan di atas dengan sebenar-benarnya
    </div>

    <div>&nbsp;</div>
        
    <div class="form-actions well">
    	<?php $this->widget('booster.widgets.TbButton', array(
    			'buttonType'=>'submit',
    			'context'=>'primary',
    			'url'=>array('site/index'),
                'icon'=>'ok',
    			'label'=>'Kirim Pengaduan',
    	)); ?>
    </div>

    </div>
</div>


<?php $this->endWidget(); ?>