<?php
	$tanggapan_class = "alert-success";

	$ikon = "komentar.png";
	if($tanggapan->id_unit_tujuan!=null)
	{
		$ikon = "disposisi.png";
	}
?>

<div style="padding:10px" class="media <?php print $tanggapan_class; ?>">

	<a class="pull-left" href="#">
		<img class="media-object" style="width: 70px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/<?php print $ikon; ?>">
	</a>

	<div class="media-body">
		<div class="media-body">
			<h4 class="media-heading">

				<?php if($tanggapan->id_unit_tujuan!=null) { ?>
				<a href="#">
					<span style="font-weight:bold">Disposisi: <?php print $tanggapan->getPembuat(); ?> <i class="glyphicon glyphicon-arrow-right"></i> <?php print $tanggapan->getUnitTujuan(); ?></span>
				</a>
				<?php } else { ?>
				<a href="#">
					<span style="font-weight:bold">Tanggapan: <?php print $tanggapan->getPembuat(); ?></span>
				</a>
				<?php } ?>
			
			</h4>
			<span style="font-weight:bold">Tanggapan: </span><?php print $tanggapan->tanggapan; ?><br>
			<span style="font-weight:bold">Waktu: </span><?php print Helper::getCreatedTime($tanggapan->waktu_dibuat); ?><br>
		</div>	
	</div>

</div>