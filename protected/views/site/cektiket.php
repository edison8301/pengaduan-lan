
<h5 style="font-weight:bold">Silakan Masukkan Kode Tiket Pengaduan Anda</h5>

<div>&nbsp;</div>

<div id="carikode">

	<?php print CHtml::beginForm(array('site/cekTiket'),'GET'); ?>

	<div class="form-group" style="margin-bottom:0px">
		<div class="row">
			<div class="col-sm-7">
				<?php print CHtml::TextField('kode','',array(
						'class'=>'form-control',
						'style'=>'width: 100%;',
						'placeholder'=>'Kode Tiket Pengaduan'
				));?>
			</div>
			<div class="col-sm-5">

				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'=>'primary',
					'icon' => 'search',
					'label'=>'Cari Pengaduan',
				)); ?>
			</div>
		</div>
	</div>



	<?php print CHtml::endForm(); ?>

</div><!-- #carikode -->


<?php if(isset($_GET['kode'])) { ?>
<?php $kode = $_GET['kode']; ?>
<?php $pengaduan = Pengaduan::model()->findByAttributes(array('kode'=>$kode)); ?>

<?php
	if($pengaduan !== null) {
		Yii::app()->user->setFlash('success','Pengaduan ditemukan, berikut ini informasi pengaduan anda');
		
	} else {
		Yii::app()->user->setFlash('danger','Mohon maaf, kode tiket yang anda masukkan tidak ditemukan dalam data pengaduan kami');		
	}
?>

<?php if($pengaduan !== null)  { ?>

<h3>Hasil Pencarian</h3>



<hr>


<div id="cek-tiket">

<h4 style="font-weight:bold">Pengaduan</h4>



<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$pengaduan,
		'type' => 'striped condensed',
		'attributes'=>array(
			'kode',
			'nama',
			'email',
			'telepon',
			array(
				'label' => 'Ditujukan',
				'value' => $pengaduan->getUnit()
			),
			'keluhan',
			array(
				'label'=>'Waktu',
				'type'=>'raw',
				'value'=>Helper::getCreatedTime($pengaduan->waktu_dibuat)
			),
		),
));?> 

<div>&nbsp</div>

<h4 style="font-weight:bold">Tanggapan</h4>

<?php  foreach($pengaduan->findAllTanggapan() as $tanggapan) { ?>
	
<?php $this->renderPartial('_tanggapan',array('tanggapan'=>$tanggapan)); ?>

<?php } ?>


<div>&nbsp</div>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Beri Tanggapan',
		'context' => 'primary',
		'icon'=>'pencil',
		'htmlOptions' => array(
			'data-toggle' => 'modal',
			'data-target' => '#myModal',
		),
)); ?>

</div><!-- #cekTiket -->

<?php $this->beginWidget('booster.widgets.TbModal',array(
		'id'=>'myModal'
)); ?>
	
	<?php print CHtml::beginForm(); ?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Kirim Tanggapan</h4>
    </div>
 
    <div class="modal-body">
		<div style="width:300px;margin-left:auto;margin-right:auto;">
			
			<?php print CHtml::label('Form Tanggapan',''); ?><Br>
			<?php print CHtml::textArea('tanggapan','',array('class'=>'form-control','rows'=>3));?>
			
		</div>
    </div>
 
    <div class="modal-footer">
        <?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'submit',
                'context' => 'success',
                'label' => 'Kirim Tanggapan',
				'icon'=>'ok',
				//'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
	<?php print CHtml::endForm(); ?>
 
<?php $this->endWidget(); 

} ?>

<?php }	 ?>