<?php
/* @var $this DeputiController */
/* @var $model Deputi */

$this->breadcrumbs=array(
	'Deputi'=>array('admin'),
	'Tambah',
);
?>

<h1>Tambah Deputi</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>