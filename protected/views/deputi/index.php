<?php
/* @var $this DeputiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deputis',
);

$this->menu=array(
	array('label'=>'Create Deputi', 'url'=>array('create')),
	array('label'=>'Manage Deputi', 'url'=>array('admin')),
);
?>

<h1>Deputis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
