<?php
$this->breadcrumbs=array(
	'Deputi'=>array('admin'),
);
?>

<h1>Kelola Deputi</h1>

<?php $this->widget('booster.widgets.TbGridView', array(
	'id'=>'deputi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => 'striped bordered condensed',
	'columns'=>array(		
		'nama',
		'username',
		'password',
		'email',
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align: right">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'label'=>'Tambah Deputi',
			'icon'=>'plus',
			'size' => 'small',
			'context'=>'primary',
			'url'=>array('create')
	)); ?>&nbsp;

</div>