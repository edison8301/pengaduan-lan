<?php
/* @var $this DeputiController */
/* @var $model Deputi */

$this->breadcrumbs=array(
	'Deputi'=>array('admin'),
	'Detail',
);
?>

<h1>Detail Deputi</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'strided bordered condensed',
	'attributes'=>array(	
		'nama',
		'username',
		'password',
		'email',
	),
)); ?>


<div>&nbsp;</div>

<div class="well">
	<?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'link',
            'url'=>array('update','id'=>$model->id),
            'context'=>'primary',
            'icon'=>'pencil',
            'label'=>'Sunting',            
    )); ?>&nbsp;
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'link',
            'url'=>array('create'),
            'context'=>'primary',
            'icon'=>'plus',
            'label'=>'Tambah',
            'htmlOptions'=>array(),
    )); ?>&nbsp;
    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'link',
        'url'=>array('admin'),
        'context'=>'primary',
        'icon'=>'list',
        'label'=>'Kelola',            
    )); ?>&nbsp;
</div>


<h2>Daftar Unit</h2>
<?php $this->widget('booster.widgets.TbButton',array(
            'buttonType'=>'link',
            'label'=>'Tambah Unit',
            'context'=>'primary',
            'icon'=>'plus white',
            'url'=>array('/DeputiUnit/create&id_deputi='.$model->id)
)); ?>&nbsp;


<div>&nbsp;</div>
<?php if (DeputiUnit::model()->findAllByAttributes(array('id_deputi'=>$model->id))) { ?>
<table width="100%" class="table table-striped table-bordered table-hover table-condensed">
<thead>
	<tr>
		<th width="3%">No</th>
		<th>Unit</th>
		<th width="3%">Aksi</th>
	</tr>
</thead>
<?php $i=1; ?>
<?php foreach(DeputiUnit::model()->findAllByAttributes(array('id_deputi'=>$model->id)) as $data){ ?>
	<tr>
		<td style="text-align:center"><?php print $i ?></td>
		<td style="text-align:left" ><?php print $data->getNamaUnit() ?></td>
		<td style="text-align:center"><?php print CHtml::link("<i class='glyphicon glyphicon-trash'></i>",array('deputiUnit/directDelete','id'=>$data->id),array('onclick'=>'return confirm("Yakin akan menghapus data?")','data-toggle'=>'tooltip','title'=>'Hapus')); ?></td>
	</tr>
<?php $i++;} ?>
</table>		
<?php }
else {
	?>
<div class="col-sm-12" style="text-align:center;font-style:italic;"> Tidak ada Unit </div>
<?php	} ?>
<div>&nbsp;</div>