<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List User','url'=>array('index')),
array('label'=>'Create User','url'=>array('create')),
);

?>

<h1>Kelola User</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'type' => 'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'username',
			'email',
			array(
				'header'=>'Set Password',
				'type'=>'raw',
				'headerHtmlOptions' =>array('style'=>'text-align: center'),
				'htmlOptions'=>array('width'=>'2','style'=>'text-align: center'),
				'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-lock\"></i>",array("user/setPassword","id"=>"$data->id"),array("data-toggle"=>"tooltip","title"=>"Set Password"))',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions' => array('style' => 'width:60px;text-align:center'),
				'template'=> '{update} {delete}'
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('user/create')
)); ?>&nbsp;

</div>