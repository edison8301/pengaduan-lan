<div class="fluid-menu">
        <div id="adminnav">
            <?php $this->widget('booster.widgets.TbNavbar',array(
                    'brand' => 'ADMIN - PENGADUAN',
                    'fixed' => false,
                    'fluid' => true,
                    'items' => array(
                        array(
                            'class' => 'booster.widgets.TbMenu',
                            'type' => 'navbar',
                            'items' => array(
                                array('label' => 'Beranda', 'url' => array('admin/index'), 'icon'=>'home'),
                                array('label' => 'Pengaduan', 'url' => array('pengaduan/admin'), 'icon'=>'bullhorn'),
                                array('label' => 'Unit', 'url' => array('unit/admin'), 'icon'=>'th-list'),
                                array('label' => 'User', 'url' => array('user/admin'), 'icon'=>'user'),
                            )
                        ),
                     array(
                        'class' => 'booster.widgets.TbMenu',
                        'type' => 'navbar',
                        'encodeLabel' => false,
                        'htmlOptions'=>array('class'=>'pull-right navbar-right'),
                        'items' => array(
                               array('label' => Yii::app()->user->id,'icon'=>'user','items'=>array(
                                    array('label'=>'Ubah Password', 'icon'=>'plus','url'=>array('unit/changePassword')),
                                    array('label'=>'Logout','url'=>array('site/logout')),
                                )),
                        )
                    )
                    )
                )
            ); ?>

        </div>
</div>
