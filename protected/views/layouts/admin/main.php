<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
	
</head>

<body>

<?php if(!Yii::app()->user->isGuest) { ?>
	<div class="fluid-menu">
	        <div id="mainnav">
	        <?php $this->widget('booster.widgets.TbNavbar',array(
	                'brand' => 'ADMIN - PENGADUAN',
	                'fixed' => false,
	                'fluid' => true,
	                'items' => array(
	                    array(
	                        'class' => 'booster.widgets.TbMenu',
	                        'type' => 'navbar',
	                        'items' => array(
	                            array('label' => 'Beranda', 'url' => array('admin/index'), 'icon'=>'home'),
	                            array('label' => 'Pengaduan', 'url' => array('pengaduan/admin'), 'icon'=>'bullhorn','visible'=>User::isAdmin()),
	                            array('label' => 'Deputi', 'url' => array('deputi/admin'), 'icon'=>'th-list','visible'=>User::isAdmin()),
	                            array('label' => 'Unit', 'url' => array('unit/admin'), 'icon'=>'th-list','visible'=>User::isAdmin()),
	                            array('label' => 'User', 'url' => array('user/admin'), 'icon'=>'user','visible'=>User::isAdmin()),
	                        )
	                    ),
		                 array(
		                    'class' => 'booster.widgets.TbMenu',
		                    'type' => 'navbar',
		                    'encodeLabel' => false,
		                    'htmlOptions'=>array('class'=>'pull-right navbar-right'),
		                    'items' => array(
		                           array('label' => Yii::app()->user->id,'icon'=>'user','items'=>array(
		                                array('label'=>'Ubah Password', 'icon'=>'plus','url'=>array('user/changePassword')),
		                                array('label'=>'Logout','url'=>array('site/logout')),
		                            )),
		                    )
		                )
	                )
	            )
	        ); ?>

	        </div>
	</div>
<?php } ?>


<div class="container">


	<div class="row">
		<div class="col-sm-12" class="margin-bottom: 200px;">
	      <div class="page-header" id="banner">
	        <div class="row">
	          <div class="" style="padding: 0px 15px 0 15px; margin-top: -35px">
				<div class="well well-sm">
					<img style="width: 70px; height: 70px; display: inline; float: left; margin-right: 20px" src="<?php print Yii::app()->baseUrl; ?>/images/logo.png">
					<h3>SIAP Lembaga Administrasi Negara</h3>
					<h6 style="font-family: Georgia; margin-top: -10px">Alamat : JL.Veteran No.10 Jakarta Pusat</h6>
	             </div>
	          </div>
	        </div>
	      </div>
		</div>
	</div>

	<div class="box-breadcumbs well" style="max-height: 60px; padding-top: 10px">
	        <?php if(isset($this->breadcrumbs)) {
	                if ( Yii::app()->controller->route !== 'site/index' )

	                    $this->breadcrumbs = array_merge(array (Yii::t('zii','<i class="icon-home"></i>')=>Yii::app()->homeUrl.'?r=site/index'), $this->breadcrumbs);
	                    $this->widget('zii.widgets.CBreadcrumbs', array(
	                            'links'=>$this->breadcrumbs,
	                            'homeLink'=>false,
	                            'encodeLabel'=>false,
	                            'htmlOptions'=>array ('class'=>'breadcrumb')
	                    ));
	        } ?>
	</div>
            <?php 
                foreach (Yii::app()->user->getFlashes() as $type => $flash) {
                echo "<div class='alert alert-".$type."'>{$flash}</div>";}
            ?>
	<div class="col-md-12">

		<div class="row" style="background-color: white; border: 1px solid #e3e3e3; border-radius: 5px; padding-bottom: 10px">

			<div class="col-md-12">

				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div>

<?php /*
<div style="padding:0px 15px;">
	<div class="row">
		<div class="col-xs-2">
			<?php $this->widget('booster.widgets.TbMenu', array(
					'type'=>'list',
					'stacked'=>true,
					'items'=>array(
						array('label'=>'Dashboard','icon'=>'th-list','url'=>array('/admin/index')),
						array('label'=>'Pengaduan','icon'=>'bullhorn','url'=>array('/pengaduan/admin')),
						array('label'=>'Laporan','icon'=>'file','url'=>array('/pengaduan/export')),
						array('label'=>'Kategori','icon'=>'folder-close','url'=>array('/kategori/admin')),
						//array('label'=>'Status','icon'=>'wrench','url'=>array('/status/admin')),
						'---',
						array('label'=>'User','icon'=>'user','url'=>array('/user/admin')),
						array('label' => '('.Yii::app()->user->id.') Logout', 'url' => array('site/logout'), 'icon'=>'off'),
					),
			)); ?>			
		</div>
		
		<div class="col-xs-10" class="margin-bottom: 200px;">
			<?php echo $content; ?>
		</div>
	</div>
*/ ?>
	<!--Footer Panel -->
	<div class="clear"></div>
	<div>&nbsp;</div>
	<div id="footer" style="text-align:center;padding:20px">
		&copy; LAN Lembaga Administrasi Negara
	</div><!-- #footer -->
</div><!--page-->
	
</div><!-- #wrapper -->

</body>
</html>
