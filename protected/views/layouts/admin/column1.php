<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/admin/main'); ?>

<div class="caption">

        <h1 class="page-title"><?php print $this->pageTitle; ?></h1>

</div>


<div  id="content">

        <?php echo $content; ?>

</div>

<?php $this->endContent(); ?>