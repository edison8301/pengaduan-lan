<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
  <div class="col-md-3">
    <?php $this->widget('booster.widgets.TbPanel',array(
          'title' => 'STATISTIK',
          'context' => 'primary',
          'headerIcon' => 'home',
          'content' => $this->renderPartial('partisipasi',array(),true)
    )); ?>
  	
    <?php $this->widget('booster.widgets.TbPanel',array(
          'title' => 'LOGIN ADMIN',
      	   'context' => 'primary',
          'headerIcon' => 'home',
          'content' => $this->renderPartial('_login_admin',array(),true)
      )); ?>

  </div>
  <div id="form">
    <div class="col-md-9">
    	<div>
      	<?php $this->widget('booster.widgets.TbBreadcrumbs',array(
              'homeLink' => 'Home',
              'links' => array('')
          )); ?>
    	</div>

    	<div>
    		<?php print $content; ?>
    	</div>
    </div>
  </div>
</div>


<?php $this->endContent(); ?>