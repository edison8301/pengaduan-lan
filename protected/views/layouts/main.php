<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/flaticon.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<?php if(!Yii::app()->user->isGuest) { ?>
<div class="fluid-menu">
        <div id="adminnav">
            <?php $this->widget('booster.widgets.TbNavbar',array(
                    'brand' => 'ADMIN - PENGADUAN',
                    'fixed' => false,
                    'fluid' => true,
                    'items' => array(
                        array(
                            'class' => 'booster.widgets.TbMenu',
                            'type' => 'navbar',
                            'items' => array(
                                array('label' => 'Beranda', 'url' => array('admin/index'), 'icon'=>'home'),
                                array('label' => 'Pengaduan', 'url' => array('pengaduan/admin'), 'icon'=>'bullhorn'),
                                array('label' => 'Unit', 'url' => array('unit/admin'), 'icon'=>'th-list','visible'=>User::isAdmin()),
                                array('label' => 'User', 'url' => array('user/admin'), 'icon'=>'user','visible'=>User::isAdmin()),
                            )
                        ),
                     array(
                        'class' => 'booster.widgets.TbMenu',
                        'type' => 'navbar',
                        'encodeLabel' => false,
                        'htmlOptions'=>array('class'=>'pull-right navbar-right'),
                        'items' => array(
                               array('label' => Yii::app()->user->id,'icon'=>'user','items'=>array(
                                    array('label'=>'Ubah Password', 'icon'=>'plus','url'=>array('unit/changePassword')),
                                    array('label'=>'Logout','url'=>array('site/logout')),
                                )),
                        )
                    )
                    )
                )
            ); ?>

        </div>
</div>
<?php } ?>

<div id="wrapper">
	<div class="container" id="page">
		<div class="row" id="header">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td rowspan="3">
                        <img src="<?php print Yii::app()->baseUrl; ?>/images/logo.png" style="width: 80px; height: 80px;">
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: bold; font-size: 28px; padding-bottom: 0px; margin-bottom: -5px">SIAP-LAN</td>
                </tr>
                <tr>
                    <td style=" padding-top: 0px; font-size: 17px;">Sistem Informasi Aspirasi & Pengaduan Lembaga Administrasi Negara</td>
                </tr>
            </table>
		</div><!-- header -->
			
		<div id="mainnav">
			<?php /*$this->widget('booster.widgets.TbNavbar',array(
                    'brand' => '',
                    'fixed' => false,	
    	            'fluid' => true,
                    'items' => array(
                        array(
                            'class' => 'booster.widgets.TbMenu',
            	            'type' => 'navbar',
                            'items' => array(
                                array('label' => 'Kirim Pengaduan', 'url' => array('site/index'), 'icon'=>'bullhorn'),
                                array('label' => 'Cek Pengaduan', 'url' => array('site/cekTiket'), 'icon'=>'search'),
                            )
                        )
                    )
            )); */?>
			<table cellspacing="0" cellpadding="0">
			
					<tr>
						<td style="padding-right: 0px; padding-left: 0px;"><?php print CHtml::link('Beranda',array('site/index')); ?></td>
						<td style="padding-right: 0px; padding-left: 0px;"><?php print CHtml::link('Cek Tiket',array('site/cekTiket')); ?></td>
                        <td style="padding-right: 0px; padding-left: 0px;"><?php print CHtml::link('Alur Pengaduan',array('site/alurPengaduan')); ?></td>
					</tr>
			
			</table>
        </div>
		<div id="content">
            <?php 
                foreach (Yii::app()->user->getFlashes() as $type => $flash) {
                echo "<div class='alert alert-".$type."'>{$flash}</div>";}
            ?>
			<?php echo $content; ?>
		</div><!-- #content -->
        <div class="footer">
        <hr>
            <div style="text-align: center">&copy; LAN  Lembaga Administrasi Negara - <?= date('Y'); ?>.</div>
        </div>
    </div><!-- container -->
</div>
</body>
</html>
