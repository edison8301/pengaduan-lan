<?php
$this->breadcrumbs=array(
	'Dashboard',
	'Tanggapan'

);

$this->menu=array(
array('label'=>'List Tanggapan','url'=>array('index')),
array('label'=>'Create Tanggapan','url'=>array('create')),
array('label'=>'Update Tanggapan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Tanggapan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Tanggapan','url'=>array('admin')),
);
?>

<h1>Pengirim : <b><?php echo $model->getPengaduan(); ?> </b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('tanggapan/update','id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered condensed',
'attributes'=>array(
		array(
			'label'=>'Pengaduan',
			'value'=>$model->getPengaduan()
			),
		'tanggapan',
		'pengirim',
		'waktu_dibuat',
),
)); ?>
