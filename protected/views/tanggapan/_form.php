<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tanggapan-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->dropDownListGroup($model,'id_pengaduan',array('widgetOptions'=>array('data' => CHtml::ListData(Pengaduan::model()->findAll(), 'id', 'nama'),'htmlOptions'=>array('class'=>'span5')))); ?>
	<div class="well">
		<?php echo $form->textAreaGroup($model,'tanggapan', array(
				'wrapperHtmlOptions'=>array('class'=>'col-sm-6'),
				'widgetOptions'=>array('htmlOptions'=>array('rows'=>3))
		)); ?>
	</div>

	<div class="form-actions well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'share-alt',
			'label'=>'Kirim Tanggapan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
