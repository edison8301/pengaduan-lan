<?php
$this->breadcrumbs=array(
	'Pegawais'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Create Pegawai','url'=>array('create')),
array('label'=>'Update Pegawai','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pegawai','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>

<h1><b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('pegawai/update','id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'nama',
		'nip',
		'username',
		'password',
		'email',
),
)); ?>
