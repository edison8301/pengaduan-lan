<?php
/* @var $this AdminController */

$this->breadcrumbs=array(
	'Dashboard',
);

?>

<?php if(User::isAdmin()) { ?>
<div class="row">
	<div class="col-sm-6">
		<?php $this->renderPartial('grafikperbulan'); ?>
	</div>

	<div class="col-sm-6">
		<?php $this->renderPartial('grafikperunit'); ?>
	</div>
</div>
<?php } ?>

<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_index',
		'ajaxUpdate'=>false
)); ?>
