<?php 
	
	$class = null; 
	$surat = 'envelope_open.png';

	if($data->waktu_dilihat == null)
	{
		$surat = 'envelope_close.png';
		$class = 'alert-danger';
	}

?>

<div class="item">
	<ul class="media-list">
		<li class="media <?php print $class; ?>" style="padding:10px;">
			<a class="pull-left" href="#">
				<img class="media-object" style="width: 90px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/<?php print $surat; ?>">
			</a>
			<div class="media-body">

				<h4 class="media-heading">
					<span style="font-weight:bold"><?php print CHtml::link($data->kode,array('pengaduan/view','id'=>$data->id)); ?></span>
					
					<?php if($data->waktu_dilihat==null) { ?>
					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Tandai Dilihat',
						'icon'=>'ok',
						'context'=>'warning',
						'size' => 'extra_small',
						'url'=>array('pengaduan/dilihat','id'=>$data->id)
					)); ?>&nbsp;
					<?php } ?>
					
					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Tanggapi',
						'icon'=>'envelope',
						'context'=>'success',
						'size' => 'extra_small',
						'url'=>array('tanggapan/create','id_pengaduan'=>$data->id)
					)); ?>&nbsp;

					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Disposisikan',
						'icon'=>'share-alt',
						'context'=>'primary',
						'size' => 'extra_small',
						'url'=>array('tanggapan/createDisposisi','id_pengaduan'=>$data->id)
					)); ?>&nbsp;

					<?php if(User::isAdmin()) { ?>
					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Hapus',
						'icon'=>'trash',
						'context'=>'danger',
						'size' => 'extra_small',
						'url'=>array('pengaduan/directDelete','id'=>$data->id),
						'htmlOptions'=>array('onclick'=>'return confirm("Yakin akan menghapus pengaduan?")')
					)); ?>&nbsp;
					<?php } ?>
				</h4>
				
				<span style="font-weight:bold">Nama: </span><?php print $data->nama; ?><br>
				<span style="font-weight:bold">Keluhan: </span><?php print $data->keluhan; ?><br>
				<span style="font-weight:bold">Tujuan: </span><?php print $data->getUnit(); ?><br>
				<span style="font-weight:bold">Waktu: </span><?php print Helper::getCreatedTime($data->waktu_dibuat); ?><br>
				<span style="font-weight:bold">Dilihat: </span><?php print Helper::getCreatedTime($data->waktu_dilihat); ?>

				<?php foreach($data->findAllTanggapan() as $tanggapan) { ?>
				<?php $this->renderPartial('_tanggapan',array('tanggapan'=>$tanggapan)); ?>
				<?php } ?>

			</div>
		</li>
	</ul>
</div>
