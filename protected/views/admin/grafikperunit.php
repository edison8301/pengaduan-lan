<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>
<script>
FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Column3d",
        "renderAt": "unit",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
        
              "xAxisName": "Unit",
              "yAxisName": "Jumlah Unit",
              "theme": "fint"
           },
          "data": 
          [ <?php print Pengaduan::getDataChartUnit(); ?> ]
             
        }
    });

    revenueChart.render();
})
</script>
<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
  'title'=>'Grafik Pengaduan per Unit',
  'context' => 'primary',
  'headerIcon'=>'signal'
)); ?>

  <div id="unit">Grafik Pengaduan per Unit <?php print Yii::app()->baseUrl; ?> </div>

<?php $this->endWidget(); ?>
