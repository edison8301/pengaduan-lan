<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/fusioncharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/fusioncharts.theme.fint.js"); ?>
<script>
FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Column3d",
        "renderAt": "bulan",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
        
              "xAxisName": "Bulan",
              "yAxisName": "Jumlah Pengaduan",
              "theme": "fint"
           },
          "data": 
          [ <?php print Pengaduan::getDataChartBulan(); ?> ]
             
        }
    });

    revenueChart.render();
})
</script>
<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
      'title'=>'Grafik Pengaduan per Bulan',
      'context' => 'primary',
      'headerIcon'=>'signal'
)); ?>
  
  <div id="bulan">Grafik Pengaduan per Bulan</div>

<?php $this->endWidget(); ?>
