<?php
					$disposisi_class = null;
					if($disposisi->waktu_dibuat == null)
					{
						if(trim($disposisi->id)==trim(Yii::app()->user->id) OR User::isAdmin())
							$disposisi_class = "alert-danger";

						if(trim($disposisi->id_unit_tujuan)==trim(Yii::app()->user->id))
							$disposisi_class = "alert-success";
					}
				?>
				<div style="padding:10px" class="media <?php print  $disposisi_class; ?>">

					<a class="pull-left" href="#">
						<img class="media-object" style="width: 70px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/disposisi.png">
					</a>
					<div class="media-body">
						<div class="media-body">
							<h4 class="media-heading">
								<a href="<?php print $this->createUrl('disposisi/view',array('id'=>$disposisi->id)); ?>">
									<span style="font-weight:bold">Disposisi: </span><?php print $disposisi->getUnitAsal(); ?> <i class="glyphicon glyphicon-arrow-right"></i> <?php print $disposisi->getUnitTujuan(); ?>
								</a>
							</h4>
							<span style="font-weight:bold">Catatan: </span><?php print $disposisi->catatan; ?><br>
							<span style="font-weight:bold">Waktu: </span><?php print Helper::getCreatedTime($disposisi->waktu_dibuat); ?>
						</div>	
					</div>
				</div>