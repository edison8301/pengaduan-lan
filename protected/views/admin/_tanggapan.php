<?php
	$tanggapan_class = "alert-success";
	if($tanggapan->waktu_dilihat == null)
	{
		$tanggapan_class = "alert-danger";
	}

	$ikon = "komentar.png";
	if($tanggapan->id_unit_tujuan!=null)
	{
		$ikon = "disposisi.png";
	}
?>

<div style="padding:10px" class="media <?php print $tanggapan_class; ?>">

<a class="pull-left" href="#">
	<img class="media-object" style="width: 70px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/<?php print $ikon; ?>">
</a>
<div class="media-body">
	<div class="media-body">
		<h4 class="media-heading">

			<?php if($tanggapan->id_unit_tujuan!=null) { ?>
			<a href="<?php print $this->createUrl('tanggapan/dilihat',array('id'=>$tanggapan->id)); ?>">
				<span style="font-weight:bold">Disposisi: <?php print $tanggapan->getPembuat(); ?> <i class="glyphicon glyphicon-arrow-right"></i> <?php print $tanggapan->getUnitTujuan(); ?></span>
			</a>
			<?php } else { ?>
			<a href="<?php print $this->createUrl('tanggapan/dilihat',array('id'=>$tanggapan->id)); ?>">
				<span style="font-weight:bold">Tanggapan: <?php print $tanggapan->getPembuat(); ?></span>
			</a>
			<?php } ?>
			
			<?php if($tanggapan->waktu_dilihat==null) { ?>
			<?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'link',
				'label'=>'Tandai Dilihat',
				'icon'=>'ok',
				'context'=>'warning',
				'size' => 'extra_small',
				'url'=>array('tanggapan/dilihat','id'=>$tanggapan->id)
			)); ?>&nbsp;
			<?php } ?>	
			
			<?php if(User::isAdmin()) { ?>
			<?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'link',
				'label'=>'Hapus',
				'icon'=>'trash',
				'context'=>'danger',
				'size' => 'extra_small',
				'url'=>array('tanggapan/directDelete','id'=>$tanggapan->id),
				'htmlOptions'=>array('onclick'=>'return confirm("Yakin akan menghapus tanggapan/disposisi?")')
			)); ?>&nbsp;
			<?php } ?>
		</h4>
		<span style="font-weight:bold">Tanggapan: </span><?php print $tanggapan->tanggapan; ?><br>
		<span style="font-weight:bold">Waktu: </span><?php print Helper::getCreatedTime($tanggapan->waktu_dibuat); ?><br>
		<span style="font-weight:bold">Dilihat: </span><?php print Helper::getCreatedTime($tanggapan->waktu_dilihat); ?>
	</div>	
</div>
</div>