<?php
$this->breadcrumbs=array(
	'Unit'=>array('admin'),
	$model->nama,
);

$this->menu=array(
array('label'=>'List Unit','url'=>array('index')),
array('label'=>'Create Unit','url'=>array('create')),
array('label'=>'Update Unit','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Unit','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Unit','url'=>array('admin')),
);
?>

<h1><?php echo $model->nama; ?></h1>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered condensed',
'attributes'=>array(
		'nama',
		'username',
),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align: right">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'label'=>'Tambah Unit',
			'icon'=>'plus',
			'size' => 'small',
			'context'=>'primary',
			'url'=>array('create')
	)); ?>&nbsp;

</div>
