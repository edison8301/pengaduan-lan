<?php
$this->breadcrumbs=array(
	'Kelola User'=>array('user/admin'),
	'Set Password',
);
?>

<h1>Set Password</h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($SetPasswordForm); ?>

	<?php echo $form->passwordFieldGroup($SetPasswordForm,'password_baru',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>
	<?php echo $form->passwordFieldGroup($SetPasswordForm,'password_baru_konfirmasi',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array(
					'options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),
					'htmlOptions'=>array('class'=>'span5')
			), 		
			'prepend'=>'<i class="glyphicon glyphicon-lock"></i>'
	)); ?>

	<div>&nbsp;</div>

	<div class="form-actions well">
		<div class="row">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'context'=>'success',
					'icon'=>'ok',
					'label'=>'Simpan',
				)); ?>
			</div>
		</div>
	</div>

	
<?php $this->endWidget(); ?>