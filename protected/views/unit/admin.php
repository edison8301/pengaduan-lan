<?php
$this->breadcrumbs=array(
	'Unit'=>array('admin'),
);
?>

<h1>Kelola Unit</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'unit-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type' => 'striped bordered condensed',
		'columns'=>array(
				'nama',
				'email',
				'username',
				array(
					'header'=>'Set Password',
					'type'=>'raw',
					'headerHtmlOptions' =>array('style'=>'text-align: center'),
					'htmlOptions'=>array('width'=>'2','style'=>'text-align: center'),
					'value'=>'CHtml::link("<i class=\"glyphicon glyphicon-lock\"></i>",array("unit/setPassword","id"=>"$data->id"),array("data-toggle"=>"tooltip","title"=>"Set Password"))',
				),
				array(
					'class'=>'booster.widgets.TbButtonColumn',
					'template'=>'{update} {delete}'
				),
		),
)); ?>


<div>&nbsp;</div>


<div class="well" style="text-align: right">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'label'=>'Tambah Unit',
			'icon'=>'plus',
			'size' => 'small',
			'context'=>'primary',
			'url'=>array('create')
	)); ?>&nbsp;

</div>