<?php
$this->breadcrumbs=array(
	'Unit'=>array('admin'),
	'Sunting',
);

	$this->menu=array(
	array('label'=>'List Unit','url'=>array('index')),
	array('label'=>'Create Unit','url'=>array('create')),
	array('label'=>'View Unit','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Unit','url'=>array('admin')),
	);
	?>

	<h1>Sunting <?php echo $model->nama; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>