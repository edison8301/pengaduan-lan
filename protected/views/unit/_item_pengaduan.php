<?php $pengaduan_class = null; if($data->waktu_dilihat == null) $pengaduan_class = 'alert-success'; ?>
<?php $surat = null; if($data->waktu_dilihat == null) $surat = 'envelope_close.png'; else $surat = 'envelope_open.png'; ?>
<div class="item">
	<ul class="media-list">
		<li class="media <?= $pengaduan_class; ?>">
			<a class="pull-left" href="#">
				<img class="media-object" style="width: 90px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/<?php print $surat; ?>">
			</a>
			<div class="media-body">

				<h4 class="media-heading">
					<?php print CHtml::link($data->kode,array('pengaduan/view','id'=>$data->id)); ?> &nbsp;
					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Tanggapi',
						'icon'=>'envelope',
						'context'=>'success',
						'size' => 'small',
						'url'=>array('tanggapan/create','id_pengaduan'=>$data->id)
					)); ?>&nbsp;

					<?php $this->widget('booster.widgets.TbButton',array(
						'buttonType'=>'link',
						'label'=>'Disposisikan',
						'icon'=>'share-alt',
						'context'=>'primary',
						'size' => 'small',
						'url'=>array('disposisi/create','id_pengaduan'=>$data->id)
					)); ?>&nbsp;
				</h4>
				<span style="font-weight:bold">Keluhan: </span><?php print $data->keluhan; ?><br>
				<span style="font-weight:bold">Waktu Input: </span><?php print Helper::getCreatedTime($data->waktu_dibuat); ?>
				<?php foreach($data->findAllTanggapan() as $tanggapan) { ?>
				<?php
					$tanggapan_class = null;
					if($tanggapan->waktu_dibuat == null)
					{
						if(trim($tanggapan->tanggapan)==trim(Yii::app()->user->id) OR User::isAdmin())
							$tanggapan_class = "alert-danger";

						if(trim($tanggapan->pengirim)==trim(Yii::app()->user->id))
							$tanggapan_class = "alert-success";
					}
				?>
				<div class="media <?php print  $tanggapan_class; ?>">

					<a class="pull-left" href="#">
						<img class="media-object" style="width: 70px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/komentar.png">
					</a>
					<div class="media-body">
						<div class="media-body">
							<h4 class="media-heading">
								<a href="<?php print $this->createUrl('tanggapan/view',array('id'=>$tanggapan->id)); ?>">
									<span style="font-weight:bold">Pengirim: </span><?php print $tanggapan->pengirim; ?>
								</a>
							</h4>
							<?php print $tanggapan->tanggapan; ?>
						</div>	
					</div>
				</div>
				<?php } ?>
				<?php foreach($data->findAllDisposisi() as $disposisi) { ?>
				<?php
					$disposisi_class = null;
					if($disposisi->waktu_dibuat == null)
					{
						if(trim($disposisi->id)==trim(Yii::app()->user->id) OR User::isAdmin())
							$disposisi_class = "alert-danger";

						if(trim($disposisi->id_unit)==trim(Yii::app()->user->id))
							$disposisi_class = "alert-success";
					}
				?>
				<div class="media <?php print  $disposisi_class; ?>">

					<a class="pull-left" href="#">
						<img class="media-object" style="width: 70px" alt="" src="<?php print Yii::app()->baseUrl; ?>/images/disposisi.png">
					</a>
					<div class="media-body">
						<div class="media-body">
							<h4 class="media-heading">
								<a href="<?php print $this->createUrl('disposisi/view',array('id'=>$disposisi->id)); ?>">
									<span style="font-weight:bold">Disposisi: </span><?php print $disposisi->catatan; ?>
								</a>
							</h4>
							<?php print $disposisi->catatan; ?>
						</div>	
					</div>
				</div>
				<?php } ?>
			</div>
		</li>
	</ul>
</div>
