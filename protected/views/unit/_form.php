<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'unit-form',
	'type' =>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>


<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255)
				)
			)
		); ?>

	<?php echo $form->textFieldGroup($model,'username',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255)
				)
			)
	); ?>

	<?php echo $form->textFieldGroup($model,'email',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-4'),
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255)
				)
			)
	); ?>

	<?php if($model->isNewRecord) { ?>
	<?php echo $form->passwordFieldGroup($model,'password',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
		'widgetOptions'=>array(
			'htmlOptions'=>array(
				'class'=>'span5',
				'maxlength'=>255)
				)
	)); ?>

	<?php } ?>


<div>&nbsp;</div>

<div class="form-actions well">
	<div class="row">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'htmlOptions'=>array('class'=>'dim'),
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Simpan',
			)); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
