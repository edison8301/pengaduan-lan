<?php
$this->breadcrumbs=array(
	'Unit'=>array('admin'),
	'Tambah',
);

$this->menu=array(
array('label'=>'List Unit','url'=>array('index')),
array('label'=>'Manage Unit','url'=>array('admin')),
);
?>

<h1>Tambah Unit</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>