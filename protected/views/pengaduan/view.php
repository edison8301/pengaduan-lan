<?php
$this->breadcrumbs=array(
	'Pengaduan'=>array('site/index'),
	$model->kode,
);

?>

<h1>Pengaduan <b><?php echo $model->nama; ?></b></h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type' => 'striped condensed',
		'attributes'=>array(
			'kode',
			'nama',
			'email',
			'telepon',
			'keluhan',
			array(
				'label'=>'Status',
				'type'=>'raw',
				'value'=>'<span class="label label-primary">'.$model->getStatus().'</span>'
			),
			array(
				'label'=>'Waktu Pengaduan',
				'type'=>'raw',
				'value'=>Helper::getCreatedTime($model->waktu_dibuat)
			),
			array(
				'label'=>'Waktu Dilihat',
				'type'=>'raw',
				'value'=>Helper::getCreatedTime($model->waktu_dilihat)
			),
		),
));?> 

<div>&nbsp;</div> 

<div class="well">

	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tanggapi',
		'icon'=>'envelope',
		'context'=>'success',
		'url'=>array('tanggapan/create','id_pengaduan'=>$model->id)
	)); ?>&nbsp;

	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Disposisikan',
		'icon'=>'share-alt',
		'context'=>'primary',
		'url'=>array('tanggapan/createDisposisi','id_pengaduan'=>$model->id)
	)); ?>&nbsp;

	<?php if(User::isAdmin()) { ?>
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Hapus',
		'icon'=>'trash',
		'context'=>'danger',
		'url'=>array('pengaduan/directDelete','id'=>$model->id),
		'htmlOptions'=>array('onclick'=>'return confirm("Yakin akan menghapus pengaduan?")')
	)); ?>&nbsp;
	<?php } ?>

</div>

<h3>Tanggapan</h3>

<?php  $i=1; foreach($model->findAllTanggapan() as $tanggapan) { ?>
	
<?php $this->renderPartial('../admin/_tanggapan',array('tanggapan'=>$tanggapan)); ?>

<?php } ?>


