<h1>Filter Pengaduan</h1>

<?php print CHtml::beginForm(array('pengaduan/exportExcel')); ?>

	<p>Filter Barang</p>


<div class="form-group">

<div class="form-group">
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    	'name'=>'tanggal_awal',
				// additional javascript options for the date picker plugin
    	'options'=>array(
        	'showAnim'=>'fold',
        	'dateFormat'=>'yy-mm-dd'
    		),
    	'htmlOptions'=>array(
        'class'=>'form-control',
        'placeholder'=>'Tanggal Awal'
    	),
	)
); ?>
</div>

<div class="form-group">
	<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    	'name'=>'tanggal_akhir',
				// additional javascript options for the date picker plugin
    	'options'=>array(
        	'showAnim'=>'fold',
        	'dateFormat'=>'yy-mm-dd'
    		),
    	'htmlOptions'=>array(
        'class'=>'form-control',
        'placeholder'=>'Tanggal Akhir'
    	),
	)); ?>
</div>

<div class="form-group">
<?php Print CHtml:: dropDownList('kategori','',CHtml::listData(Kategori::model()->findAll(),'id','nama'), array('class' => 'form-control', 'empty' => '-- Semua Kategori --')); ?>
</div>

<div class="form-group">
<?php Print CHtml:: dropDownList('status','',CHtml::listData(Status::model()->findAll(),'id','nama'),array('class'=>'form-control', 'placeholder' => 'Lokasi', 'empty' => '-- Semua Status --')); ?>
</div>


	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Proses',
		)); ?>
	</div>
<?php print CHtml::endForm(); ?>


