<?php
$this->breadcrumbs=array(
	'Pengaduan'=>array('admin'),
);
?>

<h1>Pencarian Pengaduan</h1>


<?php if(User::isAdmin()) $akses = $model->search(); ?>
<?php if(User::isUnit()) $akses = $model->pengaduanUnit(); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pengaduan-grid',
		'type' => 'striped bordered condensed',
		'dataProvider'=>$akses,
		'filter'=>$model,
		'columns'=>array(
			array(
				'name' => 'kode',
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
				'headerHtmlOptions'=>array('style'=>'text-align:center;width:100px'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'name' => 'nama',
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
			),
			array(
				'name' => 'keluhan',
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
			),
			array(
			 	'header' => 'Unit Tujuan',
			 	'name'=> 'id_unit',
			 	'value' => '$data->getUnit()',
			 	'headerHtmlOptions' => array('width' =>'15%','style'=>'text-align:center'),
			 	'htmlOptions'=>array('style'=>'text-align:center'),
			 	'filter' => Unit::getList(),
			 	'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
			),
			array(
			 	'header' => 'Status',
			 	'name'=> 'id_status',
			 	'value' => '$data->getStatus()',
			 	'headerHtmlOptions' => array('width' =>'80px','style'=>'text-align:center'),
			 	'htmlOptions'=>array('style'=>'text-align:center'),
			 	'filter' => CHtml::listData(Status::model()->findAll(array('order'=>'nama ASC')),'id','nama'),
			 	'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
			),
			array(
				'name'=>'waktu_dibuat',
				'headerHtmlOptions' => array('width' =>'15%','style'=>'text-align:center;width:150px'),
				'htmlOptions'=>array('style'=>'text-align:center'),
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template' => '{view} {delete}',
				'htmlOptions'=>array('style'=>'width:60px;text-align:center'),
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
				'visible'=>User::isAdmin()
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template' => '{view}',
				'htmlOptions'=>array('style'=>'width:60px;text-align:center'),
				'cssClassExpression' => '$data->getCssClass($data->waktu_dilihat)',
				'visible'=>User::isUnit()
			),
		),
)); ?>
