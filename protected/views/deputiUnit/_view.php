<?php
/* @var $this DeputiUnitController */
/* @var $data DeputiUnit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_deputi')); ?>:</b>
	<?php echo CHtml::encode($data->id_deputi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_unit')); ?>:</b>
	<?php echo CHtml::encode($data->id_unit); ?>
	<br />


</div>