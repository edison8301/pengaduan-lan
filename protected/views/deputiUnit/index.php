<?php
/* @var $this DeputiUnitController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deputi Units',
);

$this->menu=array(
	array('label'=>'Create DeputiUnit', 'url'=>array('create')),
	array('label'=>'Manage DeputiUnit', 'url'=>array('admin')),
);
?>

<h1>Deputi Units</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
