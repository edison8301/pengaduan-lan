<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'unit-form',
	'type' =>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->select2Group($model,'id_unit',array(
		'wrapperHtmlOptions'=>array('class'=>'col-sm-3'),
		'widgetOptions'=>array(
			'data'=>Unit::getList(),
			'htmlOptions'=>array('empty'=>'- Pilih Unit -'))
	)); ?>

<div>&nbsp;</div>

<div class="form-actions well">
	<div class="row">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'htmlOptions'=>array('class'=>'dim'),
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Simpan',
			)); ?>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>