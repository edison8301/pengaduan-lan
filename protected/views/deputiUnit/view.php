<?php
/* @var $this DeputiUnitController */
/* @var $model DeputiUnit */

$this->breadcrumbs=array(
	'Deputi Units'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DeputiUnit', 'url'=>array('index')),
	array('label'=>'Create DeputiUnit', 'url'=>array('create')),
	array('label'=>'Update DeputiUnit', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DeputiUnit', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DeputiUnit', 'url'=>array('admin')),
);
?>

<h1>View DeputiUnit #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_deputi',
		'id_unit',
	),
)); ?>
