<?php
/* @var $this DeputiUnitController */
/* @var $model DeputiUnit */

$this->breadcrumbs=array(
	'Deputi Units'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DeputiUnit', 'url'=>array('index')),
	array('label'=>'Create DeputiUnit', 'url'=>array('create')),
	array('label'=>'View DeputiUnit', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DeputiUnit', 'url'=>array('admin')),
);
?>

<h1>Update DeputiUnit <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>