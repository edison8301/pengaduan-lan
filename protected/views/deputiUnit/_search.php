<?php
/* @var $this DeputiUnitController */
/* @var $model DeputiUnit */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_deputi'); ?>
		<?php echo $form->textField($model,'id_deputi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_unit'); ?>
		<?php echo $form->textField($model,'id_unit'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->