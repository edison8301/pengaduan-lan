<?php
$this->breadcrumbs=array(
	'Kategoris'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kategori','url'=>array('index')),
array('label'=>'Create Kategori','url'=>array('create')),
array('label'=>'Update Kategori','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kategori','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kategori','url'=>array('admin')),
);
?>

<h1>Kategori <b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('kategori/update','id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
