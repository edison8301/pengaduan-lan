<?php
$this->breadcrumbs=array(
	'Kategoris'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Kategori','url'=>array('index')),
array('label'=>'Create Kategori','url'=>array('create')),
);


?>

<h1>Kelola Kategori</h1>



<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'kategori-grid',
'dataProvider'=>$model->search(),
'type' => 'striped bordered condensed',
'filter'=>$model,
'columns'=>array(
		'nama',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('style' => 'width: 80px;'),
),
),
)); ?>


<div>&nbsp;</div>

<div class="well" style="text-align:right">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('kategori/create')
)); ?>&nbsp;
</div>